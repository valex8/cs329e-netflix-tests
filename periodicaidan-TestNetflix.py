#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, parse_movie_data, predict
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

	def setUp(self):
		self.test_data1_in = "10040:\n2417853\n1207062\n2487973\n"
		self.test_data1_out = "10040:\n2.93\n3.32\n4.01\n0.97\n"

		self.test_data2_in = "1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n"
		self.test_data2_out = "1:\n3.81\n3.50\n3.46\n4.96\n3.88\n4.08\n3.55\n3.85\n4.24\n0.62\n"

		self.test_data3_in = "10015:\n975179\n829739\n1732761\n2162539\n933118\n467156\n164480\n613439\n1312973\n1769664\n"
		self.test_data3_out = "10015:\n3.85\n3.30\n2.89\n3.60\n4.02\n3.53\n4.14\n3.91\n4.03\n3.76\n0.78\n"


	# ----
	# parse_movie_data
	# ----


	def test_parse_movie_data_1(self):
		movie_id, customer_ids = parse_movie_data(self.test_data1_in)
		self.assertEqual(movie_id, 10040)
		self.assertEqual(customer_ids, [2417853, 1207062, 2487973])

	def test_parse_movie_data_2(self):
		movie_id, customer_ids = parse_movie_data(self.test_data2_in)
		self.assertEqual(movie_id, 1)
		self.assertEqual(customer_ids, [30878, 2647871, 1283744, 2488120, 317050, 1904905, 1989766, 14756, 1027056])

	def test_parse_movie_data_3(self):
		movie_id, customer_ids = parse_movie_data(self.test_data3_in)
		self.assertEqual(movie_id, 10015)
		self.assertEqual(customer_ids, [975179, 829739, 1732761, 2162539, 933118, 467156, 164480, 613439, 1312973, 1769664])


	# ----
	# predict
	# ----


	def test_predict_1(self):
		movie_id, customer_ids = parse_movie_data(self.test_data1_in)
		pred_list = []
		for customer_id in customer_ids:
			pred_list.append(predict(customer_id, movie_id))
		self.assertEqual(pred_list, [2.936611677885143, 3.3216223217841305, 4.019966246620111])

	def test_predict_2(self):
		movie_id, customer_ids = parse_movie_data(self.test_data2_in)
		pred_list = []
		for customer_id in customer_ids:
			pred_list.append(predict(customer_id, movie_id))
		self.assertEqual(pred_list, [3.8171740164678902, 3.506981992231041, 3.460289855454781, 4.965204291736229, 3.8853949427605303, 4.085793913745162, 3.552817927083909, 3.8544823355341777, 4.240356949876925])

	def test_predict_3(self):
		movie_id, customer_ids = parse_movie_data(self.test_data3_in)
		pred_list = []
		for customer_id in customer_ids:
			pred_list.append(predict(customer_id, movie_id))
		self.assertEqual(pred_list, [3.8520929606267718, 3.305757338510177, 2.896005621922731, 3.6013334465693503, 4.024956966062101, 3.537309740852562, 4.149803192209838, 3.912915481057721, 4.0398958307293515, 3.762459772623268])


	# ----
	# eval
	# ----


	def test_eval_1(self):
		r = StringIO(self.test_data1_in)
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(
			w.getvalue(), self.test_data1_out)

	def test_eval_2(self):
		r = StringIO(self.test_data2_in)
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(
			w.getvalue(), self.test_data2_out)

	def test_eval_3(self):
		r = StringIO(self.test_data3_in)
		w = StringIO()
		netflix_eval(r, w)
		self.assertEqual(
			w.getvalue(), self.test_data3_out)


# ----
# main
# ----			
if __name__ == '__main__':
	main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
