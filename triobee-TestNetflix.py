#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, predict
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.2874999999999996\n3.4605\n3.83\n0.94\n")

    def test_eval_2(self):
        r = StringIO("10000:\n200206\n523108\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10000:\n3.7\n3.7969999999999997\n0.93\n")

    def test_eval_3(self):
        r = StringIO("2666:\n2557598\n1784426\n2595026\n1286951\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "2666:\n3.1205\n2.948\n3.5454999999999997\n3.3245\n1.35\n")



    # ----
    # predict 
    # ----

    def test_predict_1(self):
        cm = 1
        cc = 2647871
        p = predict(cm, cc)
        self.assertEqual(p, 3.566)

    def test_predict_2(self):
        cm = 10651
        cc = 1819300
        p = predict(cm, cc)
        self.assertEqual(p, 3.1085)

    def test_predict_3(self):
        cm = 1002
        cc = 1685301
        p = predict(cm, cc)
        self.assertEqual(p, 3.17)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""

