#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, compute_prediction, rmse_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------


class TestNetflix (TestCase):

    # ----
    # Get the actual customer ratings
    # ----
    def test_prediction_1(self):
        current_movie = "9999"
        current_customer = "1473765"
        rating = compute_prediction(current_movie, current_customer)
        self.assertTrue(1 <= rating <= 5)

    def test_prediction_2(self):
        current_movie = "2012"
        current_customer = "1632018"
        rating = compute_prediction(current_movie, current_customer)
        self.assertTrue(1 <= rating <= 5)

    def test_prediction_3(self):
        current_movie = "499"
        current_customer = "2397266"
        rating = compute_prediction(current_movie, current_customer)
        self.assertTrue(1 <= rating <= 5)

    # ----
    # Calculate root mean square error
    # ----
    def test_rmse_1(self):
        predictions = [3, 8, 10]
        actual = [6, 2, 9]
        rmse = rmse_eval(predictions, actual)
        self.assertEqual(rmse, 3.91)

    def test_rmse_2(self):
        predictions = [88, 88, 88]
        actual = [86, 86, 86]
        rmse = rmse_eval(predictions, actual)
        self.assertEqual(rmse, 2)

    def test_rmse_3(self):
        predictions = [5, 3, 0.5]
        actual = [0.5, 3, 5]
        rmse = rmse_eval(predictions, actual)
        self.assertEqual(rmse, 3.67)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        rmse = netflix_eval(r, w)
        self.assertLess(float(rmse), 1.00)

    def test_eval_2(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n")
        w = StringIO()
        rmse = netflix_eval(r, w)
        self.assertLess(float(rmse), 1.00)

    def test_eval_3(self):
        r = StringIO(
            "10048:\n1590947\n1367805\n1293456\n1741802\n2350428\n")
        w = StringIO()
        rmse = netflix_eval(r, w)
        self.assertLess(float(rmse), 1.00)


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
